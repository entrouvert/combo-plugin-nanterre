from pathlib import Path

import nox

nox.options.reuse_venv = True


def run_hook(name, *args, **kwargs):
    for file in [Path(__name__).parent / '.nox-hooks.py', Path('~/.config/nox/eo-hooks.py').expanduser()]:
        if not file.exists():
            continue

        globals_ = {}
        exec(file.read_text(), globals_)
        hook = globals_.get(name, None)
        if hook:
            hook(*args, **kwargs)


def setup_venv(session, *packages, django_version='>=4.2,<4.3'):
    packages = [
        f'django{django_version}',
        'django-ratelimit<3',
        'git+https://git.entrouvert.org/entrouvert/debian-django-ckeditor.git',
        'git+https://git.entrouvert.org/entrouvert/publik-django-templatetags.git',
        'git+https://git.entrouvert.org/entrouvert/combo.git',
        'pre-commit',
        'psycopg2-binary',
        'pytest',
        'WebTest',
        'django-webtest',
        *packages,
    ]
    run_hook('setup_venv', session, packages)
    session.install('-e', '.', *packages, silent=False)


def hookable_run(session, *args, **kwargs):
    args = list(args)
    run_hook('run', session, args, kwargs)
    session.run(*args, **kwargs)


@nox.session
@nox.parametrize('django', ['>=4.2,<4.3'])
def tests(session, django):
    setup_venv(
        session,
        'pytest-cov',
        'pytest-django',
        django_version=django,
    )

    args = ['py.test']
    if '--coverage' in session.posargs or not session.interactive:
        while '--coverage' in session.posargs:
            session.posargs.remove('--coverage')
        args += [
            '--cov-report',
            'xml',
            '--cov-report',
            'html',
            '--cov=combo_plugin_nanterre/',
            f'--junitxml=junit-coverage.django-{django}.xml',
        ]

    args += session.posargs + ['tests/']

    hookable_run(
        session,
        *args,
        env={
            'COMBO_SETTINGS_FILE': 'tests/settings.py',
            'DB_ENGINE': 'django.db.backends.postgresql_psycopg2',
            'DJANGO_SETTINGS_MODULE': 'combo.settings',
            'SETUPTOOLS_USE_DISTUTILS': 'stdlib',
        },
    )


@nox.session
def codestyle(session):
    session.install('pre-commit')
    session.run('pre-commit', 'run', '--all-files', '--show-diff-on-failure')


@nox.session
def check_manifest(session):
    # django is only required to compile messages
    session.install('django', 'check-manifest')
    # compile messages and css
    ignores = [
        'VERSION',
    ]
    session.run('check-manifest', '--ignore', ','.join(ignores))
