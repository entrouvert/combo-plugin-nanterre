# combo-plugin-nanterre - Combo Nanterre plugin
# Copyright (C) 2017  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import re_path

from .views import qf_carte_famille, saga_retour_asynchrone, saga_retour_synchrone, saga_transaction

urlpatterns = [
    re_path(r'^_plugin/nanterre/saga-transaction/*$', saga_transaction, name='nanterre-saga-transaction'),
    re_path(
        r'^_plugin/nanterre/saga-retour-asynchrone/*$',
        saga_retour_asynchrone,
        name='nanterre-saga-retour-asynchrone',
    ),
    re_path(
        r'^_plugin/nanterre/saga-retour-synchrone/*$',
        saga_retour_synchrone,
        name='nanterre-saga-retour-synchrone',
    ),
    re_path(
        r'^_plugin/nanterre/qf-carte-famille/(?P<qf_id>\w+)/$',
        qf_carte_famille,
        name='nanterre-qf-carte-famille',
    ),
]
