import os

INSTALLED_APPS += ('combo_plugin_nanterre',)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'TEST': {
            'NAME': 'combo-plugin-nanterre-test-%s'
            % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:30],
        },
    }
}
