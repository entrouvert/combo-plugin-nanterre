import django_webtest
import pytest


@pytest.fixture
def app(request, db):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    yield django_webtest.DjangoTestApp()
    wtm._unpatch_settings()


def test_url(app):
    resp = app.get('/_plugin/nanterre/saga-retour-asynchrone/')
    assert resp.json == {'err': 1}
